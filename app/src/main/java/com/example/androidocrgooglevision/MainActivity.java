package com.example.androidocrgooglevision;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.vision.Frame;
import com.google.android.gms.vision.text.Text;
import com.google.android.gms.vision.text.TextBlock;
import com.google.android.gms.vision.text.TextRecognizer;

public class MainActivity extends AppCompatActivity {

    ImageView imageview;
    Button btnProcess, btnPria, btnWanita;
    TextView txtView;
    Bitmap bitmap;
    String ocr = "";
    String result = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageview = findViewById(R.id.image_view);
        btnProcess = findViewById(R.id.btnProcess);
        btnPria = findViewById(R.id.btnPria);
        btnWanita = findViewById(R.id.btnWanita);
        txtView = findViewById(R.id.txtView);
//        bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ocr_sample_pria);
//        imageview.setImageBitmap(bitmap);

        imageview.setVisibility(View.INVISIBLE);
        btnProcess.setVisibility(View.INVISIBLE);

        btnPria.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ocr_sample_pria);
                imageview.setImageBitmap(bitmap);
                imageview.setVisibility(View.VISIBLE);
                btnProcess.setVisibility(View.VISIBLE);
                txtView.setText("[No Text]");
            }
        });

        btnWanita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bitmap = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ocr_sample_wanita);
                imageview.setImageBitmap(bitmap);
                imageview.setVisibility(View.VISIBLE);
                btnProcess.setVisibility(View.VISIBLE);
                txtView.setText("[No Text]");
            }
        });

        btnProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextRecognizer txtRecognizer = new TextRecognizer.Builder(getApplicationContext()).build();
                if (!txtRecognizer.isOperational()) {
                    txtView.setText(R.string.error_prompt);
                } else {
                    Frame frame = new Frame.Builder().setBitmap(bitmap).build();
                    SparseArray items = txtRecognizer.detect(frame);
                    StringBuilder strBuilder = new StringBuilder();
                    for (int i = 0; i < items.size(); i++) {
                        TextBlock item = (TextBlock) items.valueAt(i);
                        strBuilder.append(item.getValue());
                        strBuilder.append("#");
                        for (Text line : item.getComponents()) {
                            //extract scanned text lines here
                            Log.v("lines", line.getValue());
                            for (Text element : line.getComponents()) {
                                //extract scanned text words here
                                Log.v("element", element.getValue());

                            }
                        }
                    }
                    ocr = strBuilder.toString().substring(0, strBuilder.toString().length() - 1);
                    //txtView.setText(ocr);
                    OCRResult();
                }
            }
        });
    }

    public void OCRResult() {
        String title = "";
        String nik = "NIK: ";
        String name = "Nama: ";
        String placeAndDOB = "Tempat/Tgl Lahir: ";
        String gender = "Jenis Kelamin: ";
        String address = "Alamat: ";
        String religion = "Agama: ";
        String status = "Status Perkawinan: ";
        String job = "Pekerjaan: ";
        String nationality = "Kewarganegaraan: ";
        String expiredDate = "Berlaku Hingga: ";

        int temp = 0;

        //title
        for (int i = 0; i < ocr.length(); i++) {
            if(ocr.charAt(i) == '#') {
                temp = i;
                break;
            }
            title += ocr.charAt(i);
        }

        //NIK
        for (int i = temp + 5; i < ocr.length(); i++) {
            if(ocr.charAt(i) == '#') {
                temp = i;
                break;
            }
            if(Character.isDigit(ocr.charAt(i)))
                nik += ocr.charAt(i);
        }

        //name
        for (int i = temp; i < ocr.length(); i++) {
            if (Character.isUpperCase(ocr.charAt(i)) && Character.isUpperCase(ocr.charAt(i + 1))) {
                name += ocr.charAt(i);
            } else if (Character.isUpperCase(ocr.charAt(i)) && ocr.charAt(i + 1) == ' ') {
                name += ocr.charAt(i);
                name += ocr.charAt(i + 1);
            } else if (Character.isUpperCase(ocr.charAt(i)) && ocr.charAt(i + 1) == '#') {
                name += ocr.charAt(i);
                temp = i+1;
                break;
            }
        }

        //PlaceAndDOB
        for (int i = temp; i < ocr.length(); i++) {
            if (Character.isUpperCase(ocr.charAt(i)) && Character.isUpperCase(ocr.charAt(i + 1))) {
                placeAndDOB += ocr.charAt(i);
            } else if (Character.isUpperCase(ocr.charAt(i)) && ocr.charAt(i + 1) == ',') {
                placeAndDOB += ocr.charAt(i);
                int flag = 0;
                for (int a = i + 1; a < ocr.length(); a++) {
                    flag++;
                    placeAndDOB += ocr.charAt(a);
                    if (flag == 12) {
                        temp = a + 1;
                        break;
                    }
                }
                break;
            }
        }

        //gender
        for (int i = temp; i < ocr.length(); i++) {
            if(ocr.charAt(i) == 'P' && ocr.charAt(i+1) == 'E' && ocr.charAt(i+2) == 'R') {
                gender += "PEREMPUAN";
                temp = i + 10;
                break;
            }

            if(ocr.charAt(i) == 'L' && ocr.charAt(i+1) == 'A' && ocr.charAt(i+2) == 'K') {
                gender += "LAKI-LAKI";
                temp = i + 10;
                break;
            }
        }

        //address
        int flag, flag2, flag3;
        flag = flag2 = flag3 = 0;
        String alamat = "";
        String RT = "";
        String RW = "";
        for (int i = temp; i < ocr.length(); i++) {
            if(ocr.charAt(i) != '#' && flag == 1) {
                alamat += ocr.charAt(i);
            } else if (ocr.charAt(i) == '#') {
                flag++;
            }

            if(flag == 2 && ocr.charAt(i) == '#') {
                temp = i + 1;
                for (int a = temp; a < ocr.length(); a++) {
                    if(Character.isDigit(ocr.charAt(a)) && flag2 < 3) {
                        RT += ocr.charAt(a);
                        flag2++;
                    }
                    if(flag2 == 3) {
                        temp = a+1;
                        break;
                    }
                }

                for (int a = temp; a < ocr.length(); a++) {
                    if(Character.isDigit(ocr.charAt(a)) && flag3 < 3) {
                        RW += ocr.charAt(a);
                        flag3++;
                    }
                    if(flag3 == 3) {
                        temp = a+1;
                        break;
                    }
                }
            }



        }
        address += alamat + "\n     RT/RW: " + RT + " / " + RW;


        result = title + "\n\n" +  nik + "\n" + name + "\n" + placeAndDOB + "\n" + gender + "\n"
                + address
                + "\n\n\n" + "========================\n\n\n" + ocr;

        txtView.setText(result);

    }

}
